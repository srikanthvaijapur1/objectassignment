function pairs(obj){
    let arr = [];
    if(typeof(obj)=='object'){
        for(let i in obj){
            arr.push(i,obj[i])
        }
    }
    return arr;
}

module.exports = pairs;