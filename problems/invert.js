function invert(obj){
    let invertObj = {}
    if(typeof(obj)=='object'){
        for(let i in obj){
            invertObj[obj[i]]=i
        }
    }
    return invertObj
}

module.exports = invert