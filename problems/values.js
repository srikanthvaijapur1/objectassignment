function values(obj){
    let objValues = [];
    if(typeof(obj)=='object'){
        for(let i in obj){
            objValues.push(obj[i])
        }
    }
    return objValues
}

module.exports = values;