function defaults(obj, defaultProps) {
    if (!obj) return {};
    if (!defaultProps) return obj;
    for (let i in defaultProps) {
        if (!obj[i]) {
            obj[i] = defaultProps[i];
            break;
        }
    }
    return obj
}

module.exports = defaults