function mapObject(obj,cb){
    let mapObj = {}
    if(typeof(obj)=='object'){
        for(let key in obj){
            mapObj[key] = cb(obj[key],key);

        }
    }
    return mapObj;
}

module.exports = mapObject;